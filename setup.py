# -*- coding: utf-8; -*-

# setup.py
# Part of ‘dput’, a Debian package upload toolkit.
#
# This is free software, and you are welcome to redistribute it under
# certain conditions; see the end of this file for copyright
# information, grant of license, and disclaimer of warranty.

""" Distribution setup for ‘dput’ library. """

from __future__ import (absolute_import, unicode_literals)

import email.utils
import os
import os.path
import pydoc

import debian.changelog
import debian.copyright
import debian.deb822
from setuptools import (setup, find_packages)


setup_dir = os.path.dirname(__file__)

readme_file_path = os.path.join(setup_dir, "README")
with open(readme_file_path) as readme_file:
    (synopsis, long_description) = pydoc.splitdoc(readme_file.read())

changelog_file_path = os.path.join(setup_dir, "debian", "changelog")
with open(changelog_file_path) as changelog_file:
    changelog = debian.changelog.Changelog(changelog_file, max_blocks=1)
(author_name, author_email) = email.utils.parseaddr(changelog.author)

control_file_path = os.path.join(setup_dir, "debian", "control")
with open(control_file_path) as control_file:
    control_structure = debian.deb822.Deb822(control_file)
(maintainer_name, maintainer_email) = email.utils.parseaddr(
        control_structure['maintainer'])

copyright_file_path = os.path.join(setup_dir, "debian", "copyright")
with open(copyright_file_path) as copyright_file:
    copyright_structure = debian.copyright.Copyright(copyright_file)
general_files_paragraph = copyright_structure.find_files_paragraph("*")
license = general_files_paragraph.license


setup(
        name=changelog.package,
        version=str(changelog.version),
        packages=find_packages(exclude=["test"]),

        # Setuptools metadata.
        maintainer=maintainer_name,
        maintainer_email=maintainer_email,
        zip_safe=False,
        setup_requires=[
            "python-debian",
            ],
        test_suite="unittest2.collector",
        tests_require=[
            "unittest2 >=0.5.1",
            "testtools",
            "testscenarios >=0.4",
            "mock >=1.3",
            "python-debian",
            "pygpgme",
            "httpretty",
            ],
        install_requires=[
            "setuptools",
            "python-debian",
            "pygpgme",
            ],
        entry_points={
            'console_scripts': [
                "execute-dput = dput.dput:main",
                "execute-dcut = dput.dcut:dcut",
                ],
            },

        # PyPI metadata.
        author=author_name,
        author_email=author_email,
        description=synopsis,
        license=license.synopsis,
        keywords="debian package upload test".split(),
        url=control_structure['homepage'],
        long_description=long_description,
        classifiers=[
            # Reference: http://pypi.python.org/pypi?%3Aaction=list_classifiers
            "Development Status :: 5 - Production/Stable",
            "License :: OSI Approved :: GNU General Public License",
            "Operating System :: POSIX",
            "Programming Language :: Python :: 2.7",
            "Programming Language :: Python :: 3",
            "Intended Audience :: Developers",
            "Topic :: Software Development :: Build Tools",
            ],
        )


# Copyright © 2008–2016 Ben Finney <ben+python@benfinney.id.au>
#
# This is free software: you may copy, modify, and/or distribute this work
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; version 3 of that license or any later version.
# No warranty expressed or implied. See the file ‘LICENSE.GPL-3’ for details.


# Local variables:
# coding: utf-8
# mode: python
# End:
# vim: fileencoding=utf-8 filetype=python :

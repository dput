#! /usr/bin/make -f
#
# This is free software, and you are welcome to redistribute it under
# certain conditions; see the end of this file for copyright
# information, grant of license, and disclaimer of warranty.

# Makefile for ‘dput’ code base.

PYTHON ?= /usr/bin/python2
PYTHON_OPTS ?= -3 -tt -bb

UNITTEST2 = /usr/bin/unit2

PY_MODULE_SUFFIX = .py
PY_MODULE_BYTECODE_SUFFIX = .pyc
package_modules = $(shell find ${CURDIR}/dput/ -name '*${PY_MODULE_SUFFIX}')
python_modules = $(shell find ${CURDIR}/ -name '*${PY_MODULE_SUFFIX}')

GENERATED_FILES :=
GENERATED_FILES += $(patsubst \
	%${PY_MODULE_SUFFIX},%${PY_MODULE_BYTECODE_SUFFIX}, \
	${python_modules})
GENERATED_FILES += ${CURDIR}/*.egg-info
GENERATED_FILES += ${CURDIR}/build ${CURDIR}/dist

UNITTEST_OPTS ?= --buffer

PYTHON_COVERAGE = $(PYTHON) -m coverage
COVERAGE_RUN_OPTS ?= --branch
COVERAGE_REPORT_OPTS ?=
COVERAGE_TEXT_REPORT_OPTS ?=
COVERAGE_HTML_REPORT_OPTS ?=


.PHONY: all
all:


.PHONY: clean
clean:
	$(RM) -r ${GENERATED_FILES}


.PHONY: tags
tags: TAGS

GENERATED_FILES += TAGS

TAGS: ${python_modules}
	etags --output "$@" --lang=python ${python_modules}


.PHONY: test
test: test-unittest

.PHONY: test-unittest
test-unittest:
	$(PYTHON) -m unittest discover ${UNITTEST_OPTS}

.PHONY: test-coverage
test-coverage: test-coverage-run test-coverage-html test-coverage-report

GENERATED_FILES += .coverage

.PHONY: test-coverage-run
test-coverage-run: coverage_opts = ${COVERAGE_RUN_OPTS}
test-coverage-run:
	$(PYTHON) -m coverage run ${coverage_opts} \
		$(UNITTEST2) discover ${UNITTEST_OPTS}

GENERATED_FILES += htmlcov/

.PHONY: test-coverage-html
test-coverage-html: coverage_opts = ${COVERAGE_REPORT_OPTS} ${COVERAGE_HTML_REPORT_OPTS}
test-coverage-html:
	$(PYTHON_COVERAGE) html ${coverage_opts} ${package_modules}

.PHONY: test-coverage-report
test-coverage-report: coverage_opts = ${COVERAGE_REPORT_OPTS} ${COVERAGE_TEXT_REPORT_OPTS}
test-coverage-report:
	$(PYTHON_COVERAGE) report ${coverage_opts} ${package_modules}


# Copyright © 2015–2016 Ben Finney <bignose@debian.org>
#
# This is free software: you may copy, modify, and/or distribute this work
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; version 3 of that license or any later version.
# No warranty expressed or implied. See the file ‘LICENSE.GPL-3’ for details.


# Local variables:
# coding: utf-8
# mode: make
# End:
# vim: fileencoding=utf-8 filetype=make :

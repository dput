# -*- coding: utf-8; -*-
#
# test/__init__.py
# Part of ‘dput’, a Debian package upload toolkit.
#
# Copyright © 2015–2016 Ben Finney <bignose@debian.org>
#
# This is free software: you may copy, modify, and/or distribute this work
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; version 3 of that license or any later version.
# No warranty expressed or implied. See the file ‘LICENSE.GPL-3’ for details.

""" Python package for Dput test suite. """


# Local variables:
# coding: utf-8
# mode: python
# End:
# vim: fileencoding=utf-8 filetype=python :
